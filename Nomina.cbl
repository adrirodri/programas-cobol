      ******************************************************************
      * Author: Adrián
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. Nomina.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       01 WS-REGTRAB OCCURS 5 TIMES INDEXED BY WS-INDEX.                Así tenemos un array de 5 elementos
           02 WS-ID            PICTURE 9(2).
           02 WS-NOMBRETRAB    PICTURE X(30).
           02 WS-APELLIDO1     PICTURE X(30).
           02 WS-APELLIDO2     PICTURE X(30).
           02 WS-SUELDO        PICTURE 9(05)V9(02).
           02 WS-DEPARTAMENTOS PICTURE X(2) OCCURS 3 TIMES.             Array de 3 elementos.
       77 WS-FIN               PICTURE 9(3).                            Contador auxiliar para mostrar los trabajadores
       77 WS-CONT              PICTURE 9(3).                            Contador de trabajadores incorporados
       77 WS-IDAUX             PICTURE 9(2).
       77 WS-COL               PICTURE 9(1).
       77 WS-ELECCION          PICTURE 9(1).
       77 WS-AUXCAMTEXT        PICTURE X(30).
       77 WS-AUXSUELDO         PICTURE 9(05)V9(02).
       77 WS-AUXDEPARTAMENTO   PICTURE X(2).

       01 WS-OP                PICTURE A(1) VALUE SPACE.
           88 WS-ALTA          VALUE 'A'.
           88 WS-BAJA          VALUE 'B'.
           88 WS-CONSG         VALUE 'C'.
           88 WS-CONST         VALUE 'T'.
           88 WS-MODT          VALUE 'M'.
           88 WS-SALIR         VALUE 'S'.
           88 WS-NO            VALUE 'N'.
           88 WS-SI            VALUE 'Y'.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
            DISPLAY "NOMINA TRABAJADORES".
            MOVE 1 TO WS-CONT
            PERFORM menu UNTIL WS-SALIR.



           menu.
            DISPLAY "A - ALTA, B - BAJA, C - CONSULTA GENERAL, T - CON"
      -     "SULTA TRABAJADOR, M - MODIFICACIÓN TRABAJADOR, S - SALIR".
            ACCEPT WS-OP.

            IF WS-ALTA THEN
               PERFORM obtencionTrabajadores                               UNTIL WS-FIN>5                Se finaliza al obtener 5 trabajadores
            ELSE IF WS-CONSG THEN
                MOVE 1 TO WS-FIN
                PERFORM mostrarTrabajadores UNTIL WS-FIN = WS-CONT
            ELSE IF WS-CONST THEN
                PERFORM mostrarTrabajador
            ELSE IF WS-MODT THEN
               PERFORM mostrarTrabajador
               DISPLAY " "
               PERFORM modificarTrabajador
            ELSE IF WS-BAJA THEN
               PERFORM mostrarTrabajador
               DISPLAY " "
               DISPLAY "CONFIRMA LA BAJA DE ESTE TRABAJADOR? Y=SI, N=NO"
               ACCEPT WS-OP
               PERFORM bajaTrabajador
            ELSE IF WS-SALIR THEN
               STOP RUN
            ELSE
               DISPLAY "OPCON INVALIDAD, VERIFIQUE"
            END-IF.


           obtencionTrabajadores.
            DISPLAY "NOMBRE DEL EMPLEADO: ".
            ACCEPT WS-NOMBRETRAB(WS-CONT).
            DISPLAY "PRIMER APELLIDO: ".
            ACCEPT WS-APELLIDO1(WS-CONT).
            DISPLAY "SEGUNDO APELLIDO: ".
            ACCEPT WS-APELLIDO2(WS-CONT).
            DISPLAY "SUELDO MENSUAL: ".
            ACCEPT WS-SUELDO(WS-CONT).
            MOVE WS-CONT TO WS-ID(WS-CONT).

            MOVE 1 TO WS-COL.
            PERFORM obtencionDepartamentos UNTIL WS-COL > 3.
            PERFORM preguntaSeguir1.


           obtencionDepartamentos.
            IF WS-COL > 3 THEN
                DISPLAY "SOLO SE PUEDEN INSERTAR 3 DEPARTAMENTOS"
                PERFORM preguntaSeguir1
            ELSE
                DISPLAY "INSERTE EL DEPARTAMENTO DEL TRABAJADOR: "
                ACCEPT WS-DEPARTAMENTOS(WS-CONT, WS-COL)
                ADD 1 TO WS-COL
            END-IF

            PERFORM otroDepartamento.

           otroDepartamento.
            DISPLAY "DESEA INSERTAR OTRO DEPARTAMENTO?: Y- SI, N- NO ".
            ACCEPT WS-OP.

            IF WS-SI THEN
               PERFORM obtencionDepartamentos
            ELSE IF WS-NO THEN
               MOVE 4 TO WS-COL
            ELSE
               DISPLAY "OPCION INVALIDA, VERIFIQUE"
               PERFORM otroDepartamento
            END-IF.

           preguntaSeguir1.
            DISPLAY "DESEA INCORPORAR OTRO TRABAJADOR?: Y- SI, N- NO ".
            ACCEPT WS-OP.
            ADD 1 TO WS-CONT.

            IF WS-SI THEN
               PERFORM obtencionTrabajadores
            ELSE IF WS-NO THEN
               PERFORM menu
            ELSE
               DISPLAY "OPCION INVALIDA, VERIFIQUE"
               PERFORM preguntaSeguir1
            END-IF.

           mostrarTrabajadores.
            DISPLAY " ".
            DISPLAY "ID DEL EMPEADO: " WS-ID(WS-FIN).
            DISPLAY "NOMBRE DEL EMPLEADO: " WS-NOMBRETRAB(WS-FIN).
            DISPLAY "PRIMER APELLIDO: " WS-APELLIDO1(WS-FIN).
            DISPLAY "SEGUNDO APELLIDO: " WS-APELLIDO2(WS-FIN).
            DISPLAY "SUELDO MENSUAL: " WS-SUELDO(WS-FIN).

            MOVE 1 TO WS-COL.
            PERFORM consultaDepartamentosGeneral UNTIL WS-COL > 3.
            ADD 1 TO WS-FIN.
            DISPLAY " ".

           consultaDepartamentosGeneral.
            IF WS-DEPARTAMENTOS(WS-FIN,WS-COL) = SPACES THEN            SPACES Es el valor cuando no tiene nada el departamento
               MOVE 4 TO WS-COL
            ELSE
               DISPLAY "Departamento " WS-COL ": "
      -        WS-DEPARTAMENTOS(WS-FIN,WS-COL)
               ADD 1 TO WS-COL
            END-IF.

           mostrarTrabajador.
            DISPLAY "INTRODUZCA EL ID DEL TRABAJADOR BUSCADO: ".
            ACCEPT WS-IDAUX.
            SET WS-INDEX TO 1.
            SEARCH WS-REGTRAB
               AT END DISPLAY "TRABAJADOR NO ENCONTRADO" PERFORM menu
               WHEN WS-ID(WS-INDEX) = WS-IDAUX

            DISPLAY " "
            DISPLAY "ID DEL EMPEADO: " WS-ID(WS-INDEX)
            DISPLAY "NOMBRE DEL EMPLEADO: " WS-NOMBRETRAB(WS-INDEX)
            DISPLAY "PRIMER APELLIDO: " WS-APELLIDO1(WS-INDEX)
            DISPLAY "SEGUNDO APELLIDO: " WS-APELLIDO2(WS-INDEX)
            DISPLAY "SUELDO MENSUAL: " WS-SUELDO(WS-INDEX)

            MOVE 1 TO WS-COL
            PERFORM consultaDepartamentosTrabajador UNTIL WS-COL > 3.
            DISPLAY " ".

           consultaDepartamentosTrabajador.
            IF WS-DEPARTAMENTOS(WS-INDEX,WS-COL) = SPACES THEN          SPACES Es el valor cuando no tiene nada el departamento
               MOVE 4 TO WS-COL
            ELSE
               DISPLAY "Departamento " WS-COL ": "
      -        WS-DEPARTAMENTOS(WS-INDEX,WS-COL)
               ADD 1 TO WS-COL
            END-IF.

           modificarTrabajador.
            DISPLAY "QUE DESEA MODIFICAR: 1.NOMBRE, 2.APELLIDO1, "
      -     "3.APELLIDO2, 4.SUELDO, 5.DEPARTAMENTOS, 6.CANCELAR".
            ACCEPT WS-ELECCION.

            IF WS-ELECCION = 1 THEN
                DISPLAY "INTRODUZCA EL NUEVO NOMBRE"
                ACCEPT WS-AUXCAMTEXT
                MOVE WS-AUXCAMTEXT TO WS-NOMBRETRAB(WS-INDEX)
            ELSE IF WS-ELECCION = 2 THEN
                DISPLAY "INTRODUZCA EL NUEVO APELLIDO1"
                ACCEPT WS-AUXCAMTEXT
                MOVE WS-AUXCAMTEXT TO WS-APELLIDO1(WS-INDEX)
            ELSE IF WS-ELECCION = 3 THEN
                DISPLAY "INTRODUZCA EL NUEVO APELLIDO2"
                ACCEPT WS-AUXCAMTEXT
                MOVE WS-AUXCAMTEXT TO WS-APELLIDO2(WS-INDEX)
            ELSE IF WS-ELECCION = 4 THEN
                DISPLAY "INTRODUZCA EL NUEVO SUELDO"
                ACCEPT WS-AUXSUELDO
                MOVE WS-AUXSUELDO TO WS-SUELDO(WS-INDEX)
            ELSE IF WS-ELECCION = 5 THEN
                MOVE 1 TO WS-COL
                PERFORM modificarDepartamentos UNTIL WS-COL > 3
            ELSE IF WS-ELECCION = 6 THEN
               PERFORM menu
            ELSE
               DISPLAY "OPCION INVALIDA, VERIFIQUE"
               PERFORM modificarTrabajador
            END-IF.
            PERFORM menu.

           modificarDepartamentos.
            IF WS-COL < 4 THEN
               DISPLAY "INTRODUZCA EL NUEVO DEPARTAMENTO " WS-COL ": "
               ACCEPT WS-AUXDEPARTAMENTO
            MOVE WS-AUXDEPARTAMENTO TO WS-DEPARTAMENTOS(WS-INDEX,WS-COL)
               ADD 1 TO WS-COL
               PERFORM modificarOtroDepartamento
            ELSE
               DISPLAY "SOLO SE PUEDEN TENER 3 DESPARTAMENTOS"
            END-IF.

           modificarOtroDepartamento.
           DISPLAY "DESEA AGREGAR OTRO DEPARTAMENTO? Y-SI, N-NO".
           ACCEPT WS-OP.

           IF WS-SI THEN
               PERFORM modificarDepartamentos
           ELSE IF WS-NO THEN
               MOVE 4 TO WS-COL
           ELSE
               DISPLAY "OPCION INVALIDA, VERIFIQUE"
               PERFORM modificarOtroDepartamento
           END-IF.

           bajaTrabajador.
            IF WS-SI THEN
                MOVE LOW-VALUES TO WS-REGTRAB(WS-INDEX)
                DISPLAY "BAJA REALIZADA"
                DISPLAY " "
                PERFORM menu
            ELSE IF WS-NO THEN
               PERFORM menu
            ELSE
               DISPLAY "CONFIRMACION ERRONEA, VERIFIQUE"
               PERFORM bajaTrabajador
            END-IF.

       END PROGRAM Nomina.
