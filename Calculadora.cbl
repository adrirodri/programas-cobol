      ******************************************************************
      * Author: Adrián
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. Calculadora.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       77 WS-N1            PIC S9(5)V9(2).                              La S es para poder tener negativos tambien
       77 WS-N2            PIC S9(5)V9(2).
       77 WS-RESULTADO     PIC S9(10)V9(2).
       77 WS-OP            PIC A(1).
       77 WS-ELEC          PIC 9(1).
       77 WS-RESFORMAT     PIC -ZZZZZZZZZ9.99.                          Para tener una variable con formato de salida apropiado
       77 WS-MOD           PIC 9(5).
       PROCEDURE DIVISION.
       MI-SECCION SECTION.
       MAIN-PROCEDURE.
            PERFORM calculadora UNTIL WS-ELEC=2.

            MOVE 15 TO WS-RESULTADO.                                    Así se pueden asignar valores a variables
            STOP RUN.






           calculadora.                                                 Rutina donde se ejecuta la calculadora
            DISPLAY "1-. USAR CALCULADORA, 2-. SALIR".                  ERASE SCREEN
            ACCEPT WS-ELEC.

            IF WS-ELEC=1 THEN
                PERFORM pedirDatos                                      Llama a la rutina de pedirDatos
                PERFORM ejecutarOperacion                               Llama a la rutina de ejecutarOperacion
            ELSE IF WS-ELEC=2 THEN
                EXIT
            ELSE
                DISPLAY "OPERACION INVALIDA, VERIFIQUE"
            END-IF.

           pedirDatos.                                                  Rutina para pedir los datos
            DISPLAY "CALCULADORA".
            DISPLAY "INSERTE EL NUMERO 1".
            ACCEPT WS-N1.
            DISPLAY "INSERTE EL NUMERO 2".
            ACCEPT WS-N2.
            DISPLAY "INDIQUE LA OPERACION A REALIZAR: S=SUMA, R=RESTA,"
      -      "M=MULTIPLICACION, D=DIVISION, L=MODULO, P=POTENCIA, "
      -      "Z=RAIZ".
            ACCEPT WS-OP.

           ejecutarOperacion.                                           Rutina para ejecutar la operación elegida
            IF FUNCTION UPPER-CASE(WS-OP)="S" THEN
               ADD WS-N1 TO WS-N2 GIVING WS-RESULTADO
            ELSE IF FUNCTION UPPER-CASE(WS-OP)="R" THEN
               SUBTRACT WS-N2 FROM WS-N1 GIVING WS-RESULTADO            El primero se resta del segundo
            ELSE IF FUNCTION UPPER-CASE(WS-OP)="M" THEN
               MULTIPLY WS-N1 BY WS-N2 GIVING WS-RESULTADO
            ELSE IF FUNCTION UPPER-CASE(WS-OP)="D" OR WS-OP="L" THEN
               DIVIDE WS-N2 INTO WS-N1 GIVING WS-RESULTADO              El divisor va antes que el dividendo
            ELSE IF FUNCTION UPPER-CASE(WS-OP)="L" THEN
               COMPUTE WS-RESULTADO = FUNCTION MOD(WS-N1, WS-N2)
            ELSE IF FUNCTION UPPER-CASE(WS-OP)="P" THEN
                COMPUTE WS-RESULTADO = WS-N1 ** WS-N2                   Potencia n2 de n1
            ELSE IF FUNCTION UPPER-CASE(WS-OP)="Z" THEN
                COMPUTE WS-RESULTADO = WS-N1 ** (1/WS-N2)               Raíz n2 de n1
            ELSE
                DISPLAY "OPERACION INVALIDA, VERIFIQUE"
            END-IF.

            IF FUNCTION UPPER-CASE(WS-OP)="S" OR "R" OR "M" OR "D"
            OR "P" OR "Z" OR "L" THEN
                MOVE WS-RESULTADO TO WS-RESFORMAT
                DISPLAY "EL RESULTADO DE LA " FUNCTION UPPER-CASE(WS-OP)
                " DE " WS-N1 " Y " WS-N2 " ES " WS-RESFORMAT
            END-IF.

                                                                        STOP "Punse cualquier tecla para continuar...".

       END PROGRAM Calculadora.
