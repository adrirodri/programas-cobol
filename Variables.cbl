      ******************************************************************
      * Author: Adrián
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. Variables.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       77 WS-NOMBRE                PICTURE A(20).
       01 WS-REGEMPLEADO.
           02 WS-NOMEMPLEADO.
               03 WS-NOMBREEPLEADO PICTURE A(15)   VALUE "PRINGAO".
               03 WS-APELLIDO1     PICTURE A(20)   VALUE "EN SALSA".
               03 WS-APELLIDO2     PICTURE A(20)   VALUE "DE TOMATE".
           02 WS-EDAD              PICTURE 9(02)   VALUE 22.
           02 WS-SUELDO            PICTURE 9(05)V9(02) VALUE 1250.95.
       66 WS-APELLIDOSEMP RENAMES WS-APELLIDO1 THRU WS-APELLIDO2.
       01 WS-BANDERA               PIC 9(1).
           88 WS-TRUE VALUE 1.
           88 WS-FALSE VALUE 0.
       01 WS-DIRECCION             PIC X(80) VALUE "Calle Era  #24".
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY "Bienvenid@ al programa de variables".
           DISPLAY "Introduce tu nombre:".
           ACCEPT WS-NOMBRE.
           DISPLAY "Hola " WS-NOMBRE.
           DISPLAY "Registro de empleado " WS-REGEMPLEADO.
           DISPLAY "NOMBRE Y APELLIDOS: " WS-NOMBREEPLEADO " "
           WS-APELLIDOSEMP.
           DISPLAY "EDAD: " WS-EDAD.
           DISPLAY "SUELDO: " WS-SUELDO.
           DISPLAY "DIRECCIÓN: " WS-DIRECCION.
           STOP "PULSE ENTER PARA FINALIZAR".
           STOP RUN.

       END PROGRAM Variables.
