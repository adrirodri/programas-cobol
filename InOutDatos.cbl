      ******************************************************************
      * Author: Adrián
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. InOutDatos.
       ENVIRONMENT DIVISION.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
       77 WS-NOMBRE  PICTURE A(20).
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
            DISPLAY "Bienvenid@ al programa".
            DISPLAY "Introduce tu nombre:".
            ACCEPT WS-NOMBRE.
            DISPLAY "Tu nombre es " WS-NOMBRE.
            STOP "PULSE ENTER PARA FINALIZAR".
            STOP RUN.
       END PROGRAM InOutDatos.
