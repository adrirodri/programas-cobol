      ******************************************************************
      * Author: Adrián
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. Textos.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
        77 WS-NOMBRE       PICTURE A(30).
        77 WS-NCONT        PICTURE 99.
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
            DISPLAY "INTRODUZCA SU NOMBRE"
            ACCEPT WS-NOMBRE

            INSPECT WS-NOMBRE TALLYING WS-NCONT FOR CHARACTERS.
            DISPLAY "LA LONGITUD DE LA VARIABLE WS-NOMBRE ES " WS-NCONT.

            MOVE ZERO TO WS-NCONT.
            INSPECT WS-NOMBRE TALLYING WS-NCONT FOR ALL 'A'.
            DISPLAY "TU NOMBRE TIENE " WS-NCONT " LETRAS 'A'".

            MOVE ZERO TO WS-NCONT.
            INSPECT WS-NOMBRE TALLYING WS-NCONT
               FOR CHARACTERS BEFORE INITIAL " ".
            DISPLAY "TU NOMBRE TIENE " WS-NCONT " CARACTERES".

            MOVE ZERO TO WS-NCONT.
            INSPECT WS-NOMBRE TALLYING WS-NCONT
               FOR LEADING "DRI".
            IF WS-NCONT = 1 THEN
               DISPLAY "TU NOMBRE NO CONTIENE LA CADENA DRI"
            ELSE
               DISPLAY "TU NOMBRE SI CONTIENE LA CADENA DRI"
            END-IF.

            INSPECT WS-NOMBRE REPLACING ALL "A" BY "O".
            DISPLAY "TU NOMBRE CAMBIANDO LAS A POR O: " WS-NOMBRE.

            INSPECT WS-NOMBRE REPLACING FIRST "O" BY "A".
            DISPLAY WS-NOMBRE.

            MOVE ZERO TO WS-NCONT
            INSPECT WS-NOMBRE TALLYING WS-NCONT FOR ALL "O"
            REPLACING ALL "O" BY "A" BEFORE INITIAL "N".
            DISPLAY "EL NOMBRE QUEDA: " WS-NOMBRE.

            INSPECT WS-NOMBRE CONVERTING "AI" TO "EY".
            DISPLAY "EL NOMBRE QUEDA: " WS-NOMBRE.

            INSPECT WS-NOMBRE CONVERTING "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"  Modo manual de pasar de mayúsculas a minúsculas
            TO "abcdefghijklmnñopqrstuvwxyz".
            DISPLAY "NOMBRE CONVERTIDO A MINUSCULAS: " WS-NOMBRE.

           STOP RUN.
       END PROGRAM Textos.
