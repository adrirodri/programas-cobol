      ******************************************************************
      * Author: Adrián
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. Archivos.
       ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
           SELECT F-ARCHIVO ASSIGN TO "DATOS.TXT".

       DATA DIVISION.
       FILE SECTION.
       FD F-ARCHIVO.
        01 REG-CLIENTE.
           02 RC-NOMBRE    PIC A(30).
           02 RC-APELLIDO1 PIC A(35).
           02 RC-APELLIDO2 PIC A(35).
           02 RC-DOMICILIO PIC X(50).
           02 RC-TELEFONO  PIC 9(9).
       WORKING-STORAGE SECTION.
       01 WS-CLIENTE.
           02 WS-NOMBREC       PIC A(100).
           02 WS-DOMICILIO     PIC X(50).
           02 WS-TELEFONO      PIC 9(9).
           02 WS-PASATIEMPO    PIC X(30).
           02 WS-COLOR         PIC X(30).
       77 WS-CONCATENADO       PIC X(150).
       PROCEDURE DIVISION.
       SECCION-PRINCIPAL SECTION.
       MAIN-PROCEDURE.
            DISPLAY "PROGRAMA GENERADOR DE ARCHIVO CLIENTES."

            DISPLAY "INGRESE EL NOMBRE COMPLETO DEL CLIENTE".
            ACCEPT WS-NOMBREC.

            DISPLAY "INGRESE LA DIRECCION DEL CLIENTE".
            ACCEPT WS-DOMICILIO.

            DISPLAY "INGRESE EL TELEFONO DEL CLIENTE".
            ACCEPT WS-TELEFONO.

            UNSTRING WS-NOMBREC DELIMITED BY " "
               INTO RC-NOMBRE RC-APELLIDO1 RC-APELLIDO2.

            DISPLAY "NOMBRE: " RC-NOMBRE.
            DISPLAY "APELLIDO(S): " RC-APELLIDO1 RC-APELLIDO2.

            MOVE WS-DOMICILIO TO RC-DOMICILIO.
            MOVE WS-TELEFONO TO RC-TELEFONO.

            OPEN OUTPUT F-ARCHIVO.
            WRITE REG-CLIENTE.

            DISPLAY "DAME PASATIEMPO FAVORITO DEL CLIENTE."
            ACCEPT WS-PASATIEMPO.
            DISPLAY "DAME COLOR FAVORITO DEL CLIENTE."
            ACCEPT WS-COLOR.

            STRING " PASATIEMPO: ", WS-PASATIEMPO, " COLOR: ", WS-COLOR
               DELIMITED BY SIZE INTO WS-CONCATENADO.

            MOVE LOW-VALUES TO REG-CLIENTE.
            WRITE REG-CLIENTE FROM WS-CONCATENADO.

            CLOSE F-ARCHIVO.
       STOP RUN.
       END PROGRAM Archivos.
